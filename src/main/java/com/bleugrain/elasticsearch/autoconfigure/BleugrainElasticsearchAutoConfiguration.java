package com.bleugrain.elasticsearch.autoconfigure;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(ElasticsearchOperations.class)
@EnableConfigurationProperties(ElasticsearchProperties.class)
@Slf4j
public class BleugrainElasticsearchAutoConfiguration {
	
	@Autowired
	private ElasticsearchProperties properties;
	
	@Bean
	@ConditionalOnMissingBean
    public RestHighLevelClient client() {
        ClientConfiguration clientConfiguration 
            = ClientConfiguration.builder()
            	.connectedTo(properties.getHost() + ":" + properties.getPort())
                .build();

        return RestClients.create(clientConfiguration).rest();
    }

    @Bean
    @ConditionalOnMissingBean
    public ElasticsearchOperations elasticsearchTemplate() {
    	log.info("defining elasticsearchTemplate bean");
        return new ElasticsearchRestTemplate(client());
    }
	
}
