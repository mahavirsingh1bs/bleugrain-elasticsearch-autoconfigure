package com.bleugrain.elasticsearch.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(prefix = "bleugrain.elasticsearch")
public class ElasticsearchProperties {
	private String host;
	private String port;
	
}
